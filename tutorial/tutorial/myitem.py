# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Item, Field


class MyItem(scrapy.Item):
		LoanNumber=scrapy.Field()
		Ssn=scrapy.Field()
		CaseUrl=scrapy.Field()
		PartyName=scrapy.Field()
		CaseNumber=scrapy.Field()
		CaseTitle=scrapy.Field()
		Court=scrapy.Field()
		Chapter=scrapy.Field()
		Dispositions=scrapy.Field()
		DateFiled=scrapy.Field()
		DateClosed=scrapy.Field()
			