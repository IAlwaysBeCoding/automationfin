import sys
reload(sys)
sys.setdefaultencoding('utf8')
import scrapy
import json
from tutorial.myitem import MyItem
from scrapy.selector import Selector
from scrapy.http.request import Request
from scrapy.selector import HtmlXPathSelector
from urlparse import urlparse
from scrapy.utils.response import open_in_browser
from tutorial.settings import *
import msvcrt
import csv
import urllib2
from urllib import urlretrieve
import urllib
import glob
from datetime import datetime
from dateutil import parser
import time

class AppURLopener(urllib.FancyURLopener):
	version = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17"

class MySpider(scrapy.Spider):
	
	name = "pcl_data"
	
	start_urls=["https://pcl.uscourts.gov/pcl/pages/search/findCase.jsf"]

	def parse(self, response):
		sel=Selector(response)

		view_state="".join(sel.xpath('//input[@name="javax.faces.ViewState"][1]/@value').extract()).strip()
		yield scrapy.FormRequest.from_response(response,
		              		formxpath='//form[@id="loginForm"]',
		                    formdata={
		                                "loginForm:loginName":"automationfinance",
										"loginForm:password":"Pa$$pass1",
										"javax.faces.partial.ajax": "true",
										"javax.faces.source": "loginForm:fbtnLogin",
										"javax.faces.partial.execute": "@all",
										"javax.faces.partial.render": "pscLoginPanel loginForm redactionConfirmation popupMsgId",
										"loginForm:fbtnLogin": "loginForm:fbtnLogin",
										"loginForm": "loginForm",
										"loginForm:clientCode": "",
										"javax.faces.ViewState": view_state

									  },
		                    clickdata={"name":"loginForm:fbtnLogin"},
		                    callback=self.after_login)

	def after_login(self,response):

		# f=open("page_source.html",'w+b')
		# f.write(response.body)
		
		yield Request("https://pcl.uscourts.gov/pcl/pages/search/findBankruptcy.jsf",callback=self.find_case)

	def find_case(self,response):
		# f=open("page_source.html",'w+b')
		# f.write(response.body)
		# open_in_browser(response)		
		sel=Selector(response)
		view_state="".join(sel.xpath('//input[@name="javax.faces.ViewState"][1]/@value').extract()).strip()
		
		fcase = open("ssn_numbers.txt",'r+b')
		case_numbers = fcase.readlines()

		for c in case_numbers:
			yield scrapy.FormRequest.from_response(response,
		              		formxpath='//form[@id="frmSearch"]',
		                    formdata={
		                    			"javax.faces.partial.ajax": "true",
										"javax.faces.source": "frmSearch:btnSearch",
										"javax.faces.partial.execute": "@all",
										"javax.faces.partial.render": "frmSearch",
										"frmSearch:btnSearch": "frmSearch:btnSearch",
										"frmSearch": "frmSearch",
										"frmSearch:txtSSN":str(c.strip())
										
									  },
							meta={'case_no':c.strip()},
		                    clickdata={"name":"frmSearch:btnSearch"},
		                    callback=self.after_search)
			# break


	def after_search(self,response):
		# sel=Selector(response)
		# f=open("page_source_"+response.meta['case_no']+"_.html",'w+b')
		# f.write(response.body)
		# open_in_browser(response)

		try:
			temp_url = response.body.split('redirect url="')[1].split('"></redirect>')[0].strip()
			if len(temp_url)>0:
				case_url="https://pcl.uscourts.gov"+temp_url

				print case_url

				yield Request(case_url,meta={'case_no':response.meta['case_no']},callback=self.case_results)
				
		except:
			item=MyItem()
			ssn=""
			try:
				if "-" not in response.meta['case_no']:
					temp_ssn=response.meta['case_no']
					ssn=temp_ssn[0]+temp_ssn[1]+temp_ssn[2]+"-"+temp_ssn[3]+temp_ssn[4]+"-"+temp_ssn[5]+temp_ssn[6]+temp_ssn[7]+temp_ssn[8]
				else:
					ssn=response.meta['case_no']
			except:
				pass	
			item['CaseNumber']="No Case Found"
			item['Ssn']=ssn
			yield item
			pass

	def case_results(self,response):

		# f=open("page_source.html",'w+b')
		# f.write(response.body)
		# open_in_browser(response)

		sel=Selector(response)

		case_links=sel.xpath('//div[@class="ui-datatable-tablewrapper"]/table/tbody/tr')
		for c in case_links:
			item=MyItem()
			ssn=""
			try:
				if "-" not in response.meta['case_no']:
					temp_ssn=response.meta['case_no']
					ssn=temp_ssn[0]+temp_ssn[1]+temp_ssn[2]+"-"+temp_ssn[3]+temp_ssn[4]+"-"+temp_ssn[5]+temp_ssn[6]+temp_ssn[7]+temp_ssn[8]
				else:
					ssn=response.meta['case_no']
			except:
				pass
			item['Ssn']=ssn
			item['CaseUrl']="".join(c.xpath('td/a[@class="ui-link ui-widget psc-command-link"]/@href').extract()).strip()
			item['PartyName']="".join(c.xpath('td[3]/span/text()').extract()).strip()
			item['CaseNumber']="".join(c.xpath('td/a[@class="ui-link ui-widget psc-command-link"]/text()').extract()).strip()
			item['CaseTitle']="".join(c.xpath('td[5]/span/text()').extract()).strip()
			item['Court']="".join(c.xpath('td[6]/span/text()').extract()).strip()
			item['Chapter']="".join(c.xpath('td[7]/span/text()').extract()).strip()
			item['Dispositions']="".join(c.xpath('td[8]/span/text()').extract()).strip()
			item['DateFiled']="".join(c.xpath('td[@class="pcl-search-results-column pcl-grid-date"][1]/span[1]/text()').extract()).strip()
			item['DateClosed']="".join(c.xpath('td[@class="pcl-search-results-column pcl-grid-date"][2]/span[1]/text()').extract()).strip()
			yield item
		
		if len(case_links)<=0:
			item=MyItem()
			item['CaseNumber']="No Case Found"
			ssn=""
			try:
				if "-" not in response.meta['case_no']:
					temp_ssn=response.meta['case_no']
					ssn=temp_ssn[0]+temp_ssn[1]+temp_ssn[2]+"-"+temp_ssn[3]+temp_ssn[4]+"-"+temp_ssn[5]+temp_ssn[6]+temp_ssn[7]+temp_ssn[8]
				else:
					ssn=response.meta['case_no']
			except:
				pass
			item['Ssn']=ssn
			yield item