

import json
import re
#from urllib.parse import urlencode
from urllib import urlencode

from scrapy import Spider, Selector, Request
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, TakeFirst, MapCompose
from scrapy.utils.project import get_project_settings

from automationfin.items import PclItem
from automationfin.utils import default_missing_keys, strip_spaces, NoneEmpty

class PclDataSpider(Spider):

    name = "pcl_data"
    allowed_domains = ['pcl.uscourts.gov' ]
    start_urls = ["https://pcl.uscourts.gov/pcl/pages/search/findCase.jsf"]


    def __init__(self, *args, **kwargs):

        settings = get_project_settings()
        ssn_file = settings['SSN_NUMBERS_FILE']
        self.username = settings['USER']
        self.password = settings['PASSWORD']

        with open(ssn_file, 'r') as f:
            self.ssn_numbers = f.readlines()

    def parse(self, response):

        post_data = {
            'javax.faces.partial.ajax' : 'true',
            'javax.faces.source' : 'loginForm:fbtnLogin',
            'javax.faces.partial.execute' : '@all',
            'javax.faces.partial.render' : 'pscLoginPanel loginForm redactionConfirmation popupMsgId',
            'loginForm:fbtnLogin' : 'loginForm:fbtnLogin',
            'loginForm' : 'loginForm',
            'loginForm:loginName' : self.username,
            'loginForm:password' : self.password,
            'loginForm:clientCode' : '',
            'javax.faces.ViewState' : response.xpath('//input[@name="javax.faces.ViewState"]/@value').extract_first()
        }

        headers = {
            'X-Requested-With' : 'XMLHttpRequest',
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
            'Faces-Request' : 'partial/ajax'
        }

        yield Request(
            'https://pcl.uscourts.gov/pcl/index.jsf',
            method='POST',
            headers=headers,
            body=urlencode(post_data),
            callback=self.process_login_attempt
        )

    def process_login_attempt(self, response):

        find_url = response.xpath('//redirect/@url').extract_first()

        if find_url:
            url = response.urljoin(find_url)
            yield Request(url, meta={'search_url' : url}, callback=self.find_cases)

    def find_cases(self, response):

        for ssn_number in self.ssn_numbers:

            post_data = {
                'javax.faces.partial.ajax' : 'true',
                'javax.faces.source' : 'frmSearch:btnSearch',
                'javax.faces.partial.execute' : '@all',
                'javax.faces.partial.render' : 'frmSearch',
                'frmSearch:btnSearch' : 'frmSearch:btnSearch',
                'frmSearch' : 'frmSearch',
                'frmSearch:txtSSN' : ssn_number.strip(),
                'javax.faces.ViewState' : response.xpath('//input[@name="javax.faces.ViewState"]/@value').extract_first()
            }
            headers = {
                'X-Requested-With' : 'XMLHttpRequest',
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
                'Faces-Request' : 'partial/ajax'
            }


            yield Request(
                response.meta['search_url'],
                method='POST',
                meta={
                    'search_url' : response.meta['search_url'],
                    'ssn' : ssn_number.strip()
                },
                body=urlencode(post_data),
                headers=headers,
                callback=self.process_ssn
            )

    def process_ssn(self, response):

        find_url = response.xpath('//redirect/@url').extract_first()

        if find_url:
            url = response.urljoin(find_url)
            yield Request(
                url,
                meta={
                    'results_url' : url,
                    'ssn' : response.meta['ssn']
                },
                callback=self.parse_ssn_result
            )
        else:
            yield self.no_case_found(response)

    def parse_ssn_result(self, response):

        cases = self.parse_ssn_cases(response)

        if cases:
            for case in cases:
                yield case
        else:
            yield self.no_case_found(response)

    def parse_ssn_cases(self, response):

        cases_raw = response.xpath('//tr[@class="ui-widget-content ui-datatable-even"]').extract()
        for case_raw in cases_raw:

            loader = ItemLoader(PclItem(), Selector(text=case_raw))
            loader.default_input_processor = MapCompose(strip_spaces)
            loader.default_output_processor = NoneEmpty()

            loader.add_value('Ssn', response.meta['ssn'])
            loader.add_xpath('CaseUrl', '//td/a[@class="ui-link ui-widget psc-command-link"]/@href')
            loader.add_xpath('PartyName', '//td[3]/span/text()')
            loader.add_xpath('CaseNumber', '//td/a[@class="ui-link ui-widget psc-command-link"]/text()')
            loader.add_xpath('CaseTitle', '//td[5]/span/text()')
            loader.add_xpath('Court', '//td[6]/span/text()')
            loader.add_xpath('Chapter', '//td[7]/span/text()')
            loader.add_xpath('Dispositions', '//td[8]/span/text()')
            loader.add_xpath('DateFiled', '//td[@class="pcl-search-results-column pcl-grid-date"][1]/span[1]/text()')
            loader.add_xpath('DateClosed','//td[@class="pcl-search-results-column pcl-grid-date"][2]/span[1]/text()')

            case_item = loader.load_item()
            default_missing_keys(case_item, default_value='')

            yield case_item

    def no_case_found(self, response):

        case_item = PclItem()
        case_item['Ssn'] = response.meta['ssn']
        case_item['CaseNumber'] = 'No Case Found'
        return case_item



