from scrapy.item import Item, Field


class PclItem(Item):

    LoanNumber = Field()
    Ssn = Field()
    CaseUrl = Field()
    PartyName = Field()
    CaseNumber = Field()
    CaseTitle = Field()
    Court = Field()
    Chapter = Field()
    Dispositions = Field()
    DateFiled = Field()
    DateClosed = Field()

