	# -*- coding: utf-8 -*-

# Scrapy settings for tutorial project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'pacer_data'

SPIDER_MODULES = ['automationfin.spiders']
NEWSPIDER_MODULE = 'automationfin.spiders'
USER = 'AutomationFinance'
PASSWORD = 'Pa$$pass1'

SSN_NUMBERS_FILE = 'ssn_numbers.txt'
FEED_EXPORT_FIELDS = ["LoanNumber","Ssn","CaseUrl","PartyName",
                      "CaseNumber","CaseTitle","Court","Chapter",
                      "Dispositions","DateFiled","DateClosed"]
# Retry many times since proxies often fail
RETRY_TIMES = 100
HTTPCACHE_ENABLED = False
# Retry on most error codes since proxies fail for different reasons
#RETRY_HTTP_CODES = [500, 503, 504, 400, 403, 404, 408]


# Proxy list containing entries like
# http://host1:port
# http://username:password@host2:port
# http://host3:port
# ...
#PROXY_LIST = 'C:/Users/mian/Desktop/yelp scrapper(rehan-zahid)/tutorial/tutorial/proxy_list.txt'

DOWNLOAD_TIMEOUT = 20000
#DUPEFILTER_DEBUG=True
#REDIRECT_ENABLED=False
# COOKIES_ENABLED = True
# HTTPCACHE_ENABLED=True
# AJAXCRAWL_ENABLED=True
USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"


# CONCURRENT_REQUESTS=1
# CONCURRENT_REQUESTS_PER_DOMAIN=1
# CONCURRENT_ITEMS=1
# DOWNLOAD_DELAY =2   #how many seconds you want your spider to wait to download other request
#PROXYMESH_ENABLED=True

ITEM_PIPELINES = {
    'automationfin.pipelines.FormatSSN': 300
}
# DOWNLOADER_MIDDLEWARES = {
#     'scproxymesh.SimpleProxymeshMiddleware': 100,
# }
#PROXYMESH_URL = 'http://fr.proxymesh.com:31280'

# Proxymesh request timeout
#PROXYMESH_TIMEOUT = 60

DOWNLOADER_MIDDLEWARES = {
#    'tutorial.randomproxy.RandomProxy': 100,
 #   'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 110,
    'scrapy.contrib.downloadermiddleware.retry.RetryMiddleware': 90,

}

# DOWNLOADER_MIDDLEWARES = {
#     'incapsula.IncapsulaMiddleware': 900
# }
# DOWNLOADER_MIDDLEWARES = {
# #	'scrapy.contrib.downloadermiddleware.retry.RetryMiddleware': 90,
#     'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 110,
# 	'tutorial.middlewares.ProxyMiddleware': 100,
# }

# DOWNLOADER_MIDDLEWARES = {
#     'scrapy.contrib.downloadermiddleware.redirect.RedirectMiddleware': None,
# }

# DOWNLOADER_MIDDLEWARES = {
#     'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': None,
#     'tutorial.middleware.RandomUserAgentMiddleware': 400,
# }

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'tutorial (+http://www.yourdomain.com)'
